#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Duy Huynh'
SITENAME = 'Duy Huynh Blog'
SITEURL = ''

PATH = 'content'

THEME = 'notmyidea'

TIMEZONE = 'Asia/Ho_Chi_Minh'

DEFAULT_LANG = 'vi'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Shoutout to PymiVN', 'https://pymi.vn'),
         ('Python.org', 'https://www.python.org/'),
         )

# Social widget
SOCIAL = (('GitHub Duyhdb', 'https://github.com/duyhdb/'),
          )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False
